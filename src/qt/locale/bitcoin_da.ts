<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>Højreklik for at redigere adresse eller mærke</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Opret en ny adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Ny</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Kopiér den valgte adresse til systemets udklipsholder</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiér</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>&amp;Luk</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;Kopiér adresse</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Slet den markerede adresse fra listen</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Eksportér den aktuelle visning til en fil</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;Eksportér</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>&amp;Slet</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Vælg adresse at sende coins til</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Vælg adresse at modtage coins med</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>&amp;Vælg</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Afsendelsesadresser</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Modtagelsesadresser</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Disse er dine Nexa-adresser for at sende betalinger. Tjek altid beløb og modtageradresse, inden du sender coins.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Dette er dine Nexa-adresser til at modtage betalinger med. Det anbefales are bruge en ny modtagelsesadresse for hver transaktion.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>Kopiér &amp;mærkat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Redigér</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Eksportér adresseliste</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Kommasepareret fil (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>Eksport mislykkedes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Der opstod en fejl under gemning af adresselisten til %1. Prøv venligst igen.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Mærkat</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(ingen mærkat)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Adgangskodedialog</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Indtast adgangskode</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Ny adgangskode</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Gentag ny adgangskode</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>Kryptér tegnebog</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Denne funktion har brug for din tegnebogs adgangskode for at låse tegnebogen op.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Lås tegnebog op</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Denne funktion har brug for din tegnebogs adgangskode for at dekryptere tegnebogen.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Dekryptér tegnebog</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Skift adgangskode</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>Bekræft tegnebogskryptering</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Advarsel: Hvis du krypterer din tegnebog og mister din adgangskode, vil du &lt;b&gt;MISTE ALLE DINE NEXA COINS&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Er du sikker på, at du ønsker at kryptere din tegnebog?</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons, previous backups of the unencrypted wallet file will become useless as soon as you start using the new, encrypted wallet.</source>
        <translation>VIGTIGT: Enhver tidligere sikkerhedskopi, som du har lavet af tegnebogsfilen, bør blive erstattet af den nyligt genererede, krypterede tegnebogsfil. Af sikkerhedsmæssige årsager vil tidligere sikkerhedskopier af den ikke-krypterede tegnebogsfil blive ubrugelige i det øjeblik, du starter med at anvende den nye, krypterede tegnebog.</translation>
    </message>
    <message>
        <location line="+104"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Advarsel: Caps Lock-tasten er aktiveret!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Tegnebog krypteret</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Indtast det nye kodeord til tegnebogen.&lt;br/&gt;Brug venligst et kodeord på &lt;b&gt;ti eller flere tilfældige tegn&lt;/b&gt; eller &lt;b&gt;otte eller flere ord&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Indtast den gamle adgangskode og en ny adgangskode til tegnebogen.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>%1 vil lukke nu for at afslutte krypteringsprocessen. Husk, at kryptering af din tegnebog ikke fuldt ud kan beskytte dine mønter mod at blive stjålet af malware, der inficerer din computer.</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Tegnebogskryptering mislykkedes</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>Tegnebogskryptering mislykkedes på grund af en intern fejl. Din tegnebog blev ikke krypteret.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>De angivne adgangskoder stemmer ikke overens.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Tegnebogsoplåsning mislykkedes</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>Den angivne adgangskode for tegnebogsdekrypteringen er forkert.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>Tegnebogsdekryptering mislykkedes</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>Tegnebogens adgangskode blev ændret.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IP/Netmaske</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>Bandlyst indtil</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>Brugeragent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>Grund til forbud</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+318"/>
        <source>Sign &amp;message...</source>
        <translation>Underskriv &amp;besked…</translation>
    </message>
    <message>
        <location line="+411"/>
        <source>Synchronizing with network...</source>
        <translation>Synkroniserer med netværk…</translation>
    </message>
    <message>
        <location line="-500"/>
        <source>&amp;Overview</source>
        <translation>&amp;Oversigt</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Knude</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Vis generel oversigt over tegnebog</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>Anmod om betalinger (genererer QR-koder og %1: URI&apos;er)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transaktioner</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Gennemse transaktionshistorik</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>E&amp;xit</source>
        <translation>&amp;Luk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Afslut program</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>&amp;Om %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>Vis oplysninger om %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>Om &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Vis informationer om Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Indstillinger…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>Rediger konfigurationsindstillinger for %1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation>&amp;Unlimited...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>Rediger Bitcoin Unlimited muligheder</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>&amp;Kryptér tegnebog…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>&amp;Sikkerhedskopiér tegnebog…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Change Passphrase...</source>
        <translation>&amp;Skift adgangskode…</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>&amp;Afsendelsesadresser…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>&amp;Modtagelsesadresser…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>&amp;Åbn URI…</translation>
    </message>
    <message>
        <location line="+397"/>
        <source>Importing blocks from disk...</source>
        <translation>Importerer blokke fra disken…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Genindekserer blokke på disken…</translation>
    </message>
    <message>
        <location line="-498"/>
        <source>Send coins to a Nexa address</source>
        <translation>Send coins til en Nexa-adresse</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Backup wallet to another location</source>
        <translation>Lav sikkerhedskopi af tegnebogen til et andet sted</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Skift adgangskode anvendt til tegnebogskryptering</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>&amp;Fejlsøgningsvindue</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Åbn fejlsøgnings- og diagnosticeringskonsollen</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;Verificér besked…</translation>
    </message>
    <message>
        <location line="+495"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-726"/>
        <source>Wallet</source>
        <translation>Tegnebog</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>&amp;Send</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>&amp;Modtag</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Vis / skjul</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Vis eller skjul hovedvinduet</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Kryptér de private nøgler, der hører til din tegnebog</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Underskriv beskeder med dine Nexa-adresser for at bevise, at de tilhører dig</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Verificér beskeder for at sikre, at de er underskrevet med de angivne Nexa-adresser</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Settings</source>
        <translation>&amp;Opsætning</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjælp</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Faneværktøjslinje</translation>
    </message>
    <message>
        <location line="-93"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Vis listen over brugte afsendelsesadresser og -mærkater</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Vis listen over brugte modtagelsesadresser og -mærkater</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>Tilvalg for &amp;kommandolinje</translation>
    </message>
    <message numerus="yes">
        <location line="+366"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n aktiv forbindelse til Nexa-netværket</numerusform>
            <numerusform>%n aktive forbindelser til Nexa-netværket</numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>Ingen blokkilde tilgængelig…</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>Bearbejdede %n blok med transaktionshistorik.</numerusform>
            <numerusform>Bearbejdede %n blokke med transaktionshistorik.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>%1 behind</source>
        <translation>%1 bagud</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>Senest modtagne blok blev genereret for %1 siden.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Transaktioner herefter vil endnu ikke være synlige.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="-85"/>
        <source>Up to date</source>
        <translation>Opdateret</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>Open a %1: URI or payment request</source>
        <translation>Åbn en %1: URI eller betalingsanmodning</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>Vis %1 hjælpemeddelelsen for at få en liste med mulige Nexa-kommandolinjeindstillinger</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>%1 client</source>
        <translation>%1 klient</translation>
    </message>
    <message>
        <location line="+251"/>
        <source>Catching up...</source>
        <translation>Indhenter…</translation>
    </message>
    <message>
        <location line="+163"/>
        <source>Date: %1
</source>
        <translation>Dato: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Beløb: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Type: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Mærkat: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Adresse: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Afsendt transaktion</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Indgående transaktion</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>Generering af HD-nøgler er &lt;b&gt;aktiveret&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>HD-nøglegenerering er &lt;b&gt;deaktiveret&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>Tegnebog er &lt;b&gt;krypteret&lt;/b&gt; og i øjeblikket &lt;b&gt;ulåst&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>Tegnebog er &lt;b&gt;krypteret&lt;/b&gt; og i øjeblikket &lt;b&gt;låst&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Coin-styring</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Mængde:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Byte:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Beløb:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Prioritet:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Gebyr:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Støv:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Efter gebyr:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Byttepenge:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>(af)vælg alle</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Trætilstand</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>Listetilstand</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Beløb</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Modtaget med mærke</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Modtaget med adresse</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Bekræftelser</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Bekræftet</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Prioritet</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Kopiér adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopiér mærkat</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Kopiér beløb</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Kopiér transaktions-ID</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>Fastlås ubrugte</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>Lås ubrugte op</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Kopiér mængde</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Kopiér gebyr</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopiér efter-gebyr</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopiér byte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopiér prioritet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Kopiér støv</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Kopiér byttepenge</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>højest</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>højere</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>højt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>mellemhøj</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>medium</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>mellemlav</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>lav</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>lavere</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>lavest</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 fastlåst)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>ingen</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Denne mærkat bliver rød, hvis transaktionsstørrelsen er større end 1000 byte.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Denne mærkat bliver rød, hvis prioriteten er mindre end &quot;medium&quot;.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Denne mærkat bliver rød, hvis en eller flere modtagere modtager et beløb, der er mindre end %1.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Kan variere med +/- %1 satoshi per input.</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>yes</source>
        <translation>ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>nej</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>Dette betyder, at et gebyr på mindst %1 pr. kB er nødvendigt.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Kan variere ±1 byte pr. input.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Transaktioner med højere prioritet har højere sansynlighed for at blive inkluderet i en blok.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(ingen mærkat)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>byttepenge fra %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(byttepange)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Redigér adresse</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>&amp;Mærkat</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>Mærkatet, der er associeret med denne indgang i adresselisten</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>Adressen, der er associeret med denne indgang i adresselisten. Denne kan kune ændres for afsendelsesadresser.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>&amp;Adresse</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Ny modtagelsesadresse</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Ny afsendelsesadresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Redigér modtagelsesadresse</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Redigér afsendelsesadresse</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>Den indtastede adresse &quot;%1&quot; er allerede i adressebogen.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>Den indtastede adresse &quot;%1&quot; er ikke en gyldig Nexa-adresse.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>Kunne ikke låse tegnebog op.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Ny nøglegenerering mislykkedes.</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>En ny datamappe vil blive oprettet.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>navn</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>Mappe eksisterer allerede. Tilføj %1, hvis du vil oprette en ny mappe her.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>Sti eksisterer allerede og er ikke en mappe.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>Kan ikke oprette en mappe her.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>version</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-bit)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>Om %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Kommandolinjetilvalg</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Anvendelse:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>kommandolinjetilvalg</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Velkommen</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>Velkommen til %1.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>Da dette er første gang programmet startes, kan du vælge, hvor %1 vil gemme sine data.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>%1 vil downloade og gemme en kopi af Nexa-blokkæden. Mindst %2 GB data vil blive gemt i denne mappe, og den vil vokse over tid. Pungen vil også blive gemt i denne mappe.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Brug standardmappen for data</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Brug tilpasset mappe for data:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Fejl: Angivet datamappe &quot;%1&quot; kan ikke oprettes.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GB fri plads tilgængelig</numerusform>
            <numerusform>%n GB fri plads tilgængelig</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(ud af %n GB behøvet)</numerusform>
            <numerusform>(ud af %n GB behøvet)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Opstrøms trafikformningsparametre må ikke være tomme</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>De viste oplysninger kan være forældede. Din tegnebog synkroniseres automatisk med Nexa-netværket, efter at der er oprettet forbindelse, men denne proces er ikke afsluttet endnu. Det betyder, at de seneste transaktioner ikke vil være synlige, og saldoen vil ikke være opdateret, før denne proces er afsluttet.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>Det er muligvis ikke muligt at bruge mønter i den fase!</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>Antal blokke tilbage</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>ukendt...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>Tidsstempel for seneste blok</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>Fremskridt</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>Fremskridtsstigning pr. time</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>beregner...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>Estimeret tid tilbage til synkronisering</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>Skjul</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>Unknown. Reindexing (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>Ukendt. Genindekserer...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>Ukendt. Synkroniserer overskrifter (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>Ukendt. Synkroniserer overskrifter...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>Åbn URI</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Åbn betalingsanmodning fra URI eller fil</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Vælg fil for betalingsanmodning</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Vælg fil for betalingsanmodning til åbning</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Indstillinger</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;Generelt</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Antallet af script&amp;verificeringstråde</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>Allow incoming connections</source>
        <translation>Tillad indkommende forbindelser</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>IP-adresse for proxyen (fx IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Minimér i stedet for at lukke applikationen, når vinduet lukkes. Når denne indstilling er slået til, vil applikationen først blive lukket, når Afslut vælges i menuen.</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>Tredjeparts-URL&apos;er (fx et blokhåndteringsværktøj), der vises i transaktionsfanen som genvejsmenupunkter. %s i URL&apos;en erstattes med transaktionens hash. Flere URL&apos;er separeres med en lodret streg |.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>Tredjeparts-transaktions-URL&apos;er</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Aktuelle tilvalg for kommandolinjen, der tilsidesætter ovenstående tilvalg:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Nulstil alle klientindstillinger til deres standard.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Nulstil indstillinger</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>&amp;Netværk</translation>
    </message>
    <message>
        <location line="-145"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = auto, &lt;0 = efterlad så mange kerner fri)</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>W&amp;allet</source>
        <translation>&amp;Tegnebog</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Ekspert</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>Slå egenskaber for &amp;coin-styring til</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Hvis du slår brug af ubekræftede byttepenge fra, kan byttepengene fra en transaktion ikke bruges, før pågældende transaktion har mindst én bekræftelse. Dette påvirker også måden hvorpå din saldo beregnes.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Brug ubekræftede byttepenge</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Når øjeblikkelige transaktioner er aktiveret, kan du bruge ubekræftede transaktioner med det samme.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>&amp;Øjeblikkelige transaktioner</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Når der oprettes og sendes transaktioner, vil automatisk konsolidering, hvis det kræves, automatisk oprette en kæde af transaktioner, som ikke har input, der er større end grænsen for konsensusinput.&lt;/p&gt;&lt;/body. &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>Automatisk konsolidering</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Åbn automatisk Nexa-klientens port på routeren. Dette virker kun, når din router understøtter UPnP, og UPnP er aktiveret.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Konfigurér port vha. &amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>Accepter forbindelser udefra.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Forbind til Nexa-netværket gennem en SOCKS5-proxy.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>&amp;Forbind gennem SOCKS5-proxy (standard-proxy):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>Proxy-&amp;IP:</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>&amp;Port:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Port for proxyen (fx 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Bruges til at nå knuder via:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Forbind til Nexa-netværket gennem en separat SOCKS5-proxy for skjulte Tor-tjenester.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Brug separat SOCKS5-proxy for at nå knuder via skjulte Tor-tjenester.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>&amp;Vindue</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Vis kun et statusikon efter minimering af vinduet.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;Minimér til statusfeltet i stedet for proceslinjen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>M&amp;inimér ved lukning</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>&amp;Visning</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>&amp;Sprog for brugergrænseflade:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>Brugergrænsefladesproget kan indstilles her. Denne indstilling træder i kraft efter genstart af %1.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>&amp;Enhed, som beløb vises i:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Vælg standard for underopdeling af enhed, som skal vises i brugergrænsefladen og ved afsendelse af coins.</translation>
    </message>
    <message>
        <location line="-502"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Hvorvidt egenskaber for coin-styring skal vises eller ej.</translation>
    </message>
    <message>
        <location line="-121"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>Start automatisk %1 efter at have logget på systemet.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>&amp;Start %1 ved systemlogin</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Start automatisk en, kun én gang, fuld databasegenindeksering ved næste opstart.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>Genindeks ved opstart</translation>
    </message>
    <message>
        <location line="+333"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>Viser, om den medfølgende standard SOCKS5-proxy bruges til at nå peers via denne netværkstype.</translation>
    </message>
    <message>
        <location line="+387"/>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annullér</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+118"/>
        <source>default</source>
        <translation>standard</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>none</source>
        <translation>ingen</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Confirm options reset</source>
        <translation>Bekræft nulstilling af indstillinger</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>Genstart af klienten er nødvendig for at aktivere ændringer.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Klienten vil lukke ned. Vil du fortsætte?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Denne ændring vil kræve en genstart af klienten.</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>Den angivne proxy-adresse er ugyldig.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>Den viste information kan være forældet. Din tegnebog synkroniserer automatisk med Nexa-netværket, når en forbindelse etableres, men denne proces er ikke gennemført endnu.</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>Kigge:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>Tilgængelig:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Din nuværende tilgængelige saldo</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>Afventende:</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Total saldo for transaktioner, som ikke er blevet bekræftet endnu, og som ikke endnu er en del af den tilgængelige saldo</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>Umodne:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Minet saldo, som endnu ikke er modnet</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>Saldi:</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>Total:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>Din nuværende totale saldo</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Din nuværende saldo på kigge-adresser</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>Spendérbar:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Nylige transaktioner</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Ubekræftede transaktioner til kigge-adresser</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Minet saldo på kigge-adresser, som endnu ikke er modnet</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Nuværende totalsaldo på kigge-adresser</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>URI-håndtering</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>Ugyldig betalingsadresse %1</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>Betalingsanmodning afvist</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>Netværk for betalingsanmodning stemmer ikke overens med klientens netværk.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Payment request is not initialized.</source>
        <translation>Betalingsanmodning er ikke klargjort.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>Anmodet betalingsbeløb på %1 er for lille (regnes som støv).</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>Fejl i betalingsanmodning</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation>Kan ikke starte click-to-pay-handler</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>Hentnings-URL for betalingsanmodning er ugyldig: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>URI kan ikke tolkes! Dette kan skyldes en ugyldig Nexa-adresse eller forkert udformede URL-parametre.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>Filhåndtering for betalingsanmodninger</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>Fil for betalingsanmodning kan ikke læses! Dette kan skyldes en ugyldig fil for betalingsanmodning.</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Payment request expired.</source>
        <translation>Betalingsanmodning er udløbet.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>Ikke-verificerede betalingsanmodninger for tilpassede betalings-scripts understøttes ikke.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>Ugyldig betalingsanmodning.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>Tilbagebetaling fra %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>Betalingsanmodning %1 er for stor (%2 byte, %3 byte tilladt).</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>Fejl under kommunikation med %1: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>Betalingsanmodning kan ikke tolkes!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>Fejlagtigt svar fra server %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>Betaling anerkendt</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>Fejl i netværksforespørgsel</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+106"/>
        <source>User Agent</source>
        <translation>Brugeragent</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>Knude/tjeneste</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Ping-tid</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Beløb</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>Indtast en NEXA-adresse (f.eks. %1)</translation>
    </message>
    <message>
        <location line="+832"/>
        <source>%1 d</source>
        <translation>%1 d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 t</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n sekund</numerusform>
            <numerusform>%n sekunder</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n minut</numerusform>
            <numerusform>%n minutter</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n time</numerusform>
            <numerusform>%n timer</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n dag</numerusform>
            <numerusform>%n dage</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n uge</numerusform>
            <numerusform>%n uger</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 og %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n år</numerusform>
            <numerusform>%n flere år</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Gem billede…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>&amp;Kopiér foto</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>Gem QR-kode</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG-billede (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>Klientversion</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;Information</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>Fejlsøgningsvindue</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>Generelt</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>Bruger BerkeleyDB version</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Startup time</source>
        <translation>Opstartstidspunkt</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>Netværk</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Antal forbindelser</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>Blokkæde</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>Nuværende antal blokke</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Modtaget</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Sendt</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>Andre &amp;knuder</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>Bandlyste knuder</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+855"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Vælg en anden knude for at se detaljeret information.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>På hvidliste</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>Retning</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Starting Block</source>
        <translation>Startblok</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>Synkroniserede headers</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>Synkroniserede blokke</translation>
    </message>
    <message>
        <location line="-2488"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>Brugeragent</translation>
    </message>
    <message>
        <location line="-2370"/>
        <source>Datadir</source>
        <translation>Datamappe</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Last block time (time since)</source>
        <translation>Sidste blokeringstid (tid siden)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>Sidste blokstørrelse</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Transaktioner i Tx-puljen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>Transaktioner i forældreløs pool</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>Beskeder i CAPD-puljen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Tx pool - brug</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Tx-pulje - txns per sekund</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin (Totaler)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin (24-timers gennemsnit)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>Kompakt (Totaler)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>Kompakt (24-timers gennemsnit)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>Grafen (Totaler)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>Grafen (24-timers gennemsnit)</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>Åbn %1 debug-logfilen fra den aktuelle datamappe. Dette kan tage et par sekunder for store logfiler.</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation>Blok udbredelse</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>Transaktionspuljer</translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation>Formindsk skriftstørrelse</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>Forøg skriftstørrelsen</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation>&amp;Transaktionsrate</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>Øjeblikkelig hastighed (1 sek.)</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>Spids</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>Runtime</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24 timer</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>Vises</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>Udjævnet hastighed (60s)</translation>
    </message>
    <message>
        <location line="+769"/>
        <source>Services</source>
        <translation>Tjenester</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Ban Score</source>
        <translation>Bandlysningsscore</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Forbindelsestid</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Seneste afsendelse</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Seneste modtagelse</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Ping-tid</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>Varigheden af den aktuelt igangværende ping.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Ping-ventetid</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Tidsforskydning</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>&amp;Åbn</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>&amp;Konsol</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>&amp;Netværkstrafik</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;Ryd</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Totaler</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>Indkommende:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Udgående:</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>Fejlsøgningslogfil</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>Ryd konsol</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation>&amp;Afbryd forbindelse til knude</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Bandlys knude i</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 &amp;time</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 &amp;dag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 &amp;uge</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 &amp;år</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>&amp;Fjern bandlysning af knude</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>Velkommen til %1 RPC-konsollen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>Brug op- og ned-piletasterne til at navigere i historikken og &lt;b&gt;Ctrl-L&lt;/b&gt; til at rydde skærmen.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Tast &lt;b&gt;help&lt;/b&gt; for en oversigt over de tilgængelige kommandoer.</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>handicappet</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(knude-id: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>via %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>aldrig</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Indkommende</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Udgående</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Ukendt</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+110"/>
        <source>&amp;Amount:</source>
        <translation>&amp;Beløb:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Label:</source>
        <translation>&amp;Mærkat:</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>&amp;Message:</source>
        <translation>&amp;Besked:</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Genbrug en af de tidligere brugte modtagelsesadresser. Genbrug af adresser har indflydelse på sikkerhed og privatliv. Brug ikke dette med mindre du genskaber en betalingsanmodning fra tidligere.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>&amp;Genbrug en eksisterende modtagelsesadresse (anbefales ikke)</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+23"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>En valgfri besked, der føjes til betalingsanmodningen, og som vil vises, når anmodningen åbnes. Bemærk: Beskeden vil ikke sendes sammen med betalingen over Nexa-netværket.</translation>
    </message>
    <message>
        <location line="-63"/>
        <location line="+56"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Et valgfrit mærkat, der associeres med den nye modtagelsesadresse.</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Brug denne formular for at anmode om betalinger. Alle felter er &lt;b&gt;valgfri&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-56"/>
        <location line="+7"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Et valgfrit beløb til anmodning. Lad dette felt være tomt eller indeholde nul for at anmode om et ikke-specifikt beløb.</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Clear all fields of the form.</source>
        <translation>Ryd alle felter af formen.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Ryd</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Requested payments history</source>
        <translation>Historik over betalingsanmodninger</translation>
    </message>
    <message>
        <location line="-197"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Anmod om betaling</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Coin freezing locks coins to make them temporarily unspendable. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Møntfrysning låser mønter for at gøre dem midlertidigt ubrugelige. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Coin &amp;Freeze</source>
        <translation>Mønt &amp;Frys</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Vis den valgte anmodning (gør det samme som dobbeltklik på en indgang)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Fjern de valgte indgange fra listen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Fjern</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+53"/>
        <source>Copy URI</source>
        <translation>Kopiér URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopiér mærkat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Kopiér besked</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopier beløb</translation>
    </message>
</context>
<context>
    <name>ReceiveFreezeDialog</name>
    <message>
        <location filename="../forms/receivefreezedialog.ui" line="+14"/>
        <source>Coin Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>WARNING! Freezing coins means they will be UNSPENDABLE until the release date or block specified below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Freeze until block :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify the block when coins are released from the freeze. Coins are UNSPENDABLE until the freeze block.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+99"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-57"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Freeze until date and time :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Specify the future date and time when coins are released from the freeze. Coins are UNSPENDABLE until after the freeze date and time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&amp;Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;Ok</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+O</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR-kode</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>Kopiér &amp;URI</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>Kopiér &amp;adresse</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Gem billede…</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Anmod om betaling til %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Betalingsinformation</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Beløb</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Mærkat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Besked</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Freeze until</source>
        <translation>Frys indtil</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>Resulterende URI var for lang; prøv at forkorte teksten til mærkaten/beskeden.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Fejl ved kodning fra URI til QR-kode.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Mærkat</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Besked</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>Beløb</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(ingen mærkat)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(ingen besked)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(intet beløb)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+627"/>
        <source>Send Coins</source>
        <translation>Send Coins</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Egenskaber for coin-styring</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Inputs...</source>
        <translation>Inputs…</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>valgt automatisk</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Utilstrækkelige midler!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Mængde:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Byte:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Beløb:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Prioritet:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Gebyr:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>Efter gebyr:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Byttepenge:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Hvis dette aktiveres, men byttepengeadressen er tom eller ugyldig, vil byttepenge blive sendt til en nygenereret adresse.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom change address</source>
        <translation>Tilpasset byttepengeadresse</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Transaction Fee:</source>
        <translation>Transaktionsgebyr:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose...</source>
        <translation>Vælg…</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>sammenfold gebyropsætning</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>pr. kilobyte</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Hvis det brugertilpassede gebyr er sat til 1000 satoshis, og transaktionen kun fylder 250 byte, betaler &quot;pr. kilobyte&quot; kun 250 satoshis i gebyr, mens &quot;total mindst&quot; betaler 1000 satoshis. For transaktioner større end en kilobyte betaler begge pr. kilobyte.</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>Skjul</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>total mindst</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Det er helt fint kun at betale det minimale gebyr, så længe den totale transaktionsvolumen er mindre end den plads, der er tilgængelig i blokkene. Men vær opmærksom på, at dette kan ende ud i transaktioner, der aldrig bliver bekræftet, når der bliver større forespørgsel efter nexa-transaktioner, end hvad netværket kan bearbejde.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(læs værktøjstippet)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Recommended:</source>
        <translation>Anbefalet:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Custom:</source>
        <translation>Brugertilpasset:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Smart-gebyr er ikke initialiseret endnu. Dette tager typisk nogle få blokke…)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Confirmation time:</source>
        <translation>Bekræftelsestid:</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>normal</source>
        <translation>normal</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>hurtig</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Send som nul-gebyr-transaktion hvis muligt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(bekræftelse kan tage længere)</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>Send til flere modtagere på en gang</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Tilføj &amp;modtager</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>Ryd alle felter af formen.</translation>
    </message>
    <message>
        <location line="-858"/>
        <source>Dust:</source>
        <translation>Støv:</translation>
    </message>
    <message>
        <location line="+861"/>
        <source>Clear &amp;All</source>
        <translation>Ryd &amp;alle</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Balance:</source>
        <translation>Saldo:</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>Confirm the send action</source>
        <translation>Bekræft afsendelsen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>&amp;Afsend</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>Bekræft afsendelse af coins</translation>
    </message>
    <message>
        <location line="-59"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 til %2</translation>
    </message>
    <message>
        <location line="-283"/>
        <source>Copy quantity</source>
        <translation>Kopiér mængde</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopier beløb</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Kopiér gebyr</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopiér efter-gebyr</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopiér byte</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopiér prioritet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy change</source>
        <translation>Kopiér byttepenge</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;Offentlig etiket:&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING!!! DESTINATION IS A FREEZE ADDRESS&lt;br&gt;UNSPENDABLE UNTIL&lt;/b&gt; %1 &lt;br&gt;*************************************************&lt;br&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;b&gt;ADVARSEL!!! DESTINATION ER EN FRYSADRESSE&lt;br&gt;KAN IKKE BRUGE INDTIL&lt;/b&gt; %1 &lt;br&gt;********************************* ******************&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Total Amount %1</source>
        <translation>Totalbeløb %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>eller</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>Beløbet til betaling skal være større end 0.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>Beløbet overstiger din saldo.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>Totalen overstiger din saldo, når transaktionsgebyret på %1 er inkluderet.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction creation failed!</source>
        <translation>Oprettelse af transaktion mislykkedes!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>Transaktionen blev afvist! Dette kan ske, hvis nogle af dine coins i din tegnebog allerede er brugt, som hvis du brugte en kopi af wallet.dat og dine coins er blevet brugt i kopien, men ikke er markeret som brugt her.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Et gebyr højere end %1 opfattes som et absurd højt gebyr.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Betalingsanmodning er udløbet.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>Public Label overskrider grænsen på </translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Pay only the required fee of %1</source>
        <translation>Betal kun det påkrævede gebyr på %1</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>Modtageradressen er ikke gyldig. Tjek venligst igen.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>Adressegenganger fundet. Adresser bør kun bruges én gang hver.</translation>
    </message>
    <message>
        <location line="+255"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Advarsel: Ugyldig Nexa-adresse</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation>Bekræft tilpasset ændringsadresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>Den adresse, du valgte til ændring, er ikke en del af denne tegnebog. Enhver eller alle midler i din tegnebog kan blive sendt til denne adresse. Er du sikker?</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(ingen mærkat)</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>Advarsel: Ukendt byttepengeadresse</translation>
    </message>
    <message>
        <location line="-781"/>
        <source>Copy dust</source>
        <translation>Kopiér støv</translation>
    </message>
    <message>
        <location line="+292"/>
        <source>Are you sure you want to send?</source>
        <translation>Er du sikker på, at du vil sende?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>tilføjet som transaktionsgebyr</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+82"/>
        <location line="+650"/>
        <location line="+533"/>
        <source>A&amp;mount:</source>
        <translation>&amp;Beløb:</translation>
    </message>
    <message>
        <location line="-1170"/>
        <source>Pay &amp;To:</source>
        <translation>Betal &amp;til:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>Vælg tidligere brugt adresse</translation>
    </message>
    <message>
        <location line="-106"/>
        <source>This is a normal payment.</source>
        <translation>Dette er en normal betaling.</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Private Description:</source>
        <translation>Privat beskrivelse:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>Privat label:</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Nexa-adresse, som betalingen skal sendes til</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Indsæt adresse fra udklipsholderen</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+526"/>
        <location line="+533"/>
        <source>Remove this entry</source>
        <translation>Fjern denne indgang</translation>
    </message>
    <message>
        <location line="-1037"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>En meddelelse, der var knyttet til mønten: URI, som vil blive gemt sammen med transaktionen til din reference. Bemærk: Denne besked vil ikke blive sendt over Nexa-netværket.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>Indtast en privat etiket for denne adresse for at tilføje den til listen over brugte adresser</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>Indtast en offentlig etiket for denne transaktion</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>Offentligt mærke:</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>Gebyret vil blive trukket fra det sendte beløb. Modtageren vil modtage færre coin, end du indtaster i beløbfeltet. Hvis flere modtagere vælges, vil gebyret deles ligeligt.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>&amp;Træk gebyr fra beløb</translation>
    </message>
    <message>
        <location line="+616"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Dette er en uautentificeret betalingsanmodning.</translation>
    </message>
    <message>
        <location line="+529"/>
        <source>This is an authenticated payment request.</source>
        <translation>Dette er en autentificeret betalingsanmodning.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>En besked, som blev føjet til “%1”-URI&apos;en, som vil gemmes med transaktionen til din reference. Bemærk: Denne besked vil ikke blive sendt over Nexa-netværket.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>Indtast en privat etiket for denne adresse for at tilføje den til din adressebog</translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-514"/>
        <location line="+529"/>
        <source>Pay To:</source>
        <translation>Betal til:</translation>
    </message>
    <message>
        <location line="-495"/>
        <location line="+533"/>
        <source>Memo:</source>
        <translation>Memo:</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>%1 lukker ned...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>Luk ikke computeren ned, før dette vindue forsvinder.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Signature - Underskriv/verificér en besked</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>&amp;Underskriv besked</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Du kan signere beskeder/aftaler med dine adresser for at bevise, at du kan modtage coin, der bliver sendt til adresserne. Vær forsigtig med ikke at signere noget vagt eller tilfældigt, da eventuelle phishing-angreb kan snyde dig til at overlade din identitet til dem. Signér kun fuldt ud detaljerede udsagn, som du er enig i.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>Nexa-adresse, som beskeden skal signeres med</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Vælg tidligere brugt adresse</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Indsæt adresse fra udklipsholderen</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Indtast her beskeden, du ønsker at underskrive</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Underskrift</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Kopiér den nuværende underskrift til systemets udklipsholder</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Underskriv denne besked for at bevise, at Nexa-adressen tilhører dig</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>Underskriv &amp;besked</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Nulstil alle &quot;underskriv besked&quot;-felter</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>Ryd &amp;alle</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>&amp;Verificér besked</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>Indtast modtagerens adresse, besked (vær sikker på at kopiere linjeskift, mellemrum, tabuleringer, etc. præcist) og signatur herunder for at verificere beskeden. Vær forsigtig med ikke at læse noget ud fra signaturen, som ikke står i selve beskeden, for at undgå at blive snydt af et eventuelt man-in-the-middle-angreb. Bemærk, at dette kun beviser, at den signerende person kan modtage med adressen; det kan ikke bevise hvem der har sendt en given transaktion!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>Nexa-adressen, som beskeden blev signeret med</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Verificér beskeden for at sikre, at den er underskrevet med den angivne Nexa-adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>Verificér &amp;besked</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Nulstil alle &quot;verificér besked&quot;-felter</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Klik &quot;Underskriv besked&quot; for at generere underskriften</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>Tegnebogsoplåsning annulleret.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Besked underskrevet.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Besked verificeret.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnetværk]</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+34"/>
        <source>Open until %1</source>
        <translation>Åben indtil %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>konflikt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/offline</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/ubekræftet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 bekræftelser</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, transmitteret igennem %n knude</numerusform>
            <numerusform>, transmitteret igennem %n knuder</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Kilde</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Genereret</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+128"/>
        <source>From</source>
        <translation>Fra</translation>
    </message>
    <message>
        <location line="-148"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+114"/>
        <source>To</source>
        <translation>Til</translation>
    </message>
    <message>
        <location line="-215"/>
        <source>double spent</source>
        <translation>dobbelt brugt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>forladt</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>change address</source>
        <translation>ændre adresse</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>egen adresse</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+119"/>
        <source>watch-only</source>
        <translation>kigge</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+28"/>
        <location line="+113"/>
        <source>label</source>
        <translation>mærkat</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+63"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>Offentligt mærke:</translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Freeze until</source>
        <translation>Frys indtil</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+74"/>
        <source>Credit</source>
        <translation>Kredit</translation>
    </message>
    <message numerus="yes">
        <location line="-222"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>modner efter yderligere %n blok</numerusform>
            <numerusform>modner efter yderligere %n blokke</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>ikke accepteret</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+79"/>
        <source>Debit</source>
        <translation>Debet</translation>
    </message>
    <message>
        <location line="-98"/>
        <source>Total debit</source>
        <translation>Total debet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Total kredit</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Transaktionsgebyr</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Nettobeløb</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+12"/>
        <source>Message</source>
        <translation>Besked</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction Idem</source>
        <translation>Transaktion Idem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>Transaktionsstørrelse</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Forretningsdrivende</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Minede coins skal modne %1 blokke, før de kan bruges. Da du genererede denne blok, blev den transmitteret til netværket for at blive føjet til blokkæden. Hvis det ikke lykkes at få den i kæden, vil dens tilstand ændres til &quot;ikke accepteret&quot;, og den vil ikke kunne bruges. Dette kan ske nu og da, hvis en anden knude udvinder en blok inden for nogle få sekunder fra din.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Debug information</source>
        <translation>Fejlsøgningsinformation</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transaktion</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Input</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Amount</source>
        <translation>Beløb</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>sand</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>falsk</translation>
    </message>
    <message>
        <location line="-357"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, er ikke blevet transmitteret endnu</translation>
    </message>
    <message numerus="yes">
        <location line="-40"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Åbn yderligere %n blok</numerusform>
            <numerusform>Åbn yderligere %n blokke</numerusform>
        </translation>
    </message>
    <message>
        <location line="+73"/>
        <source>unknown</source>
        <translation>ukendt</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Transaktionsdetaljer</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Denne rude viser en detaljeret beskrivelse af transaktionen</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>tps</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Umoden (%1 bekræftelser; vil være tilgængelig efter %2)</translation>
    </message>
    <message numerus="yes">
        <location line="-29"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Åbn yderligere %n blok</numerusform>
            <numerusform>Åbn yderligere %n blokke</numerusform>
        </translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Address or Label</source>
        <translation>Adresse eller etiket</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>Åben indtil %1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Bekræftet (%1 bekræftelser)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Denne blok blev ikke modtaget af nogen andre knuder og vil formentlig ikke blive accepteret!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Genereret, men ikke accepteret</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Ubekræftet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Bekræfter (%1 af %2 anbefalede bekræftelser)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>Konflikt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>Dobbelt brugt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>Forladt</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Modtaget med</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Modtaget fra</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Sendt til</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Betaling til dig selv</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Minet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>Offentligt mærke</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>Andet</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>kigge</translation>
    </message>
    <message>
        <location line="+276"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Transaktionsstatus. Hold musen over dette felt for at vise antallet af bekræftelser.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Dato og klokkeslæt for modtagelse af transaktionen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Transaktionstype.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Afgør hvorvidt en kigge-adresse er involveret i denne transaktion.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Brugerdefineret hensigt/formål med transaktionen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Beløb trukket fra eller tilføjet balance.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>I dag</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Denne uge</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Denne måned</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Sidste måned</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Dette år</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Interval…</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Modtaget med</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Sendt til</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Til dig selv</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Minet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Andet</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Indtast adresse eller mærkat for at søge</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Minimumsbeløb</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Kopiér adresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopiér mærkat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopiér beløb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy raw transaction</source>
        <translation>Kopiér rå transaktion</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Redigér mærkat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Vis transaktionsdetaljer</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Export Transaction History</source>
        <translation>Historik for eksport af transaktioner</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Watch-only</source>
        <translation>Kigge</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Exporting Failed</source>
        <translation>Eksport mislykkedes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>En fejl opstod under gemning af transaktionshistorik til %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Eksport problemfri</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>Transaktionshistorikken blev gemt til %1 med succes.</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Kommasepareret fil (*.csv)</translation>
    </message>
    <message>
        <location line="-175"/>
        <source>Copy transaction idem</source>
        <translation>Kopiér transaktionsidem</translation>
    </message>
    <message>
        <location line="+184"/>
        <source>Confirmed</source>
        <translation>Bekræftet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Dato</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Mærkat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Range:</source>
        <translation>Interval:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>til</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Enhed, som beløb vises i. Klik for at vælge en anden enhed.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation>Unlimited</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>&amp;Minedrift</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>Den største blok, der vil blive udvundet</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>Maksimal genereret blokstørrelse (bytes) </translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>&amp;Netværk</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>Båndbreddebegrænsninger i KBytes/sek. (marker for at aktivere):</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>Sende</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>Maks</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>Gennemsnit</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>Modtage</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>Aktuelle tilvalg for kommandolinjen, der tilsidesætter ovenstående tilvalg:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>Nulstil alle klientindstillinger til deres standard.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Nulstil indstillinger</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annullér</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>Bekræft nulstilling af indstillinger</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>Dette er en global nulstilling af alle indstillinger!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>Genstart af klienten er nødvendig for at aktivere ændringer.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Klienten vil lukke ned. Vil du fortsætte?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>Opstrøms trafikformningsparametre må ikke være tomme</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>Trafikformningsparametre skal være større end 0.</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+26"/>
        <source>No wallet has been loaded.</source>
        <translation>Ingen tegnebog er indlæst.</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+287"/>
        <source>Send Coins</source>
        <translation>Send Coins</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+44"/>
        <source>&amp;Export</source>
        <translation>&amp;Eksportér</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Eksportér den aktuelle visning til en fil</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Backup Wallet</source>
        <translation>Sikkerhedskopiér tegnebog</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Tegnebogsdata (*.dat)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Backup Failed</source>
        <translation>Sikkerhedskopiering mislykkedes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Der skete en fejl under gemning af tegnebogsdata til %1.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Tegnebogsdata blev gemt til %1 med succes.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>Sikkerhedskopiering problemfri</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+56"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>Beskæring er sat under minimumsgrænsen på %d MiB. Brug venligst et større tal.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Beskæring: Seneste synkronisering rækker udover beskårne data. Du er nødt til at bruge -reindex (downloade hele blokkæden igen i fald af beskåret knude)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation>Genindlæsninger er ikke mulige i beskåret tilstand. Du er nødt til at bruge -reindex, hvilket vil downloade hele blokkæden igen.</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Fejl: En alvorlig intern fejl er opstået. Se debug.log for detaljer</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Pruning blockstore...</source>
        <translation>Beskærer bloklager…</translation>
    </message>
    <message>
        <location line="-155"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Distribueret under MIT-softwarelicensen; se den vedlagte fil COPYING eller &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>Blokdatabasen indeholder en blok, som ser ud til at være fra fremtiden. Dette kan skyldes, at din computers dato og tid ikke er sat korrekt. Genopbyg kun blokdatabasen, hvis du er sikker på, at din computers dato og tid er korrekt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Dette er en foreløbig testudgivelse - brug på eget ansvar - brug ikke til udvinding eller handelsprogrammer</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>ADVARSEL: unormalt mange blokke er genereret; %d blokke er modtaget i løbet af de seneste %d timer (%d forventet)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>ADVARSEL: tjek din netværksforbindelse; %d blokke er modtaget i løbet af de seneste %d timer (%d forventet)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Advarsel: Netværket ser ikke ud til at være fuldt ud enige! Enkelte minere ser ud til at opleve problemer.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Advarsel: Vi ser ikke ud til at være fuldt ud enige med andre knuder! Du kan være nødt til at opgradere, eller andre knuder kan være nødt til at opgradere.</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Corrupted block database detected</source>
        <translation>Ødelagt blokdatabase opdaget</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Ønsker du at genopbygge blokdatabasen nu?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>Klargøring af blokdatabase mislykkedes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Klargøring af tegnebogsdatabasemiljøet %s mislykkedes!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>Åbning af blokdatabase mislykkedes</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>Fejl: Mangel på ledig diskplads!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Lytning på enhver port mislykkedes. Brug -listen=0, hvis du ønsker dette.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>Importerer…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Ukorrekt eller ingen tilblivelsesblok fundet. Forkert datamappe for netværk?</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Ugyldig -onion adresse: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>For få tilgængelige fildeskriptorer.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>Beskæring kan ikke opsættes med en negativ værdi.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>Beskæringstilstand er ikke kompatibel med -txindex.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>Brugeragent-kommentar (%s) indeholder usikre tegn.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Verificerer blokke…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Verificerer tegnebog…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>Venter på Genesis Block...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>Tegnebog %s findes uden for datamappe %s</translation>
    </message>
    <message>
        <location line="-166"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Fejl: Lytning efter indkommende forbindelser mislykkedes (lytning resultarede i fejl %s)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Transaktionsbeløbet er for lille til at sende, når gebyret er trukket fra</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Dette produkt indeholder software, der er udviklet af OpenSSL-projektet for brug i OpenSSL-værktøjskassen &lt;https://www.openssl.org/&gt;, samt kryptografisk software, der er skrevet af Eric Young, samt UPnP-software, der er skrevet af Thomas Bernard.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation>Du er nødt til at genopbygge databasen ved hjælp af -reindex for at gå tilbage til ikke-beskåret tilstand. Dette vil downloade hele blokkæden igen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Activating best chain...</source>
        <translation>Aktiverer bedste kæde…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>Kan ikke løse -whitebind adresse: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>Ophavsret © 2015-%i Udviklerne af Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Information</source>
        <translation>Information</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Ugyldig netmaske angivet i -whitelist: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>Nødt til at angive en port med -whitebinde: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Signing transaction failed</source>
        <translation>Underskrift af transaktion mislykkedes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>Starter txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Transaktionsbeløbet er for lille til at betale gebyret</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Dette er eksperimentelt software.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Transaktionsbeløb er for lavt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Transaktionsbeløb skal være positive</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>Transaktionen har %d udgange. Maksimalt tilladte output er %d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>Transaktionen på %d bytes er for stor. Maksimalt tilladt er %d bytes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Transaktion for stor til gebyrretningslinjer</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>Ikke i stand til at tildele til %s på denne computer (bind returnerede fejl %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Loading addresses...</source>
        <translation>Indlæser adresser…</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Ugyldig -proxy adresse: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>Ukendt netværk anført i -onlynet: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>Kan ikke finde -bind adressen: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>Kan ikke finde -externalip adressen: &quot;%s&quot;</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Loading block index...</source>
        <translation>Indlæser blokindeks…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Indlæser tegnebog…</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Cannot downgrade wallet</source>
        <translation>Kan ikke nedgradere tegnebog</translation>
    </message>
    <message>
        <location line="-125"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>%s udviklerne</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>Bitcoin Bitcoin XT og Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee er sat meget højt! Så store gebyrer kan betales på en enkelt transaktion.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee er sat meget højt! Dette er det transaktionsgebyr, du betaler, hvis du sender en transaktion.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>Kan ikke få en lås på databiblioteket %s. %s kører sandsynligvis allerede.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>Kunne ikke finde RPC-legitimationsoplysninger. Der blev ikke fundet en godkendelsescookie, og der er ikke angivet noget rpcpassword i konfigurationsfilen (%s)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>Installationskonfigurationsfilen &apos;%s&apos; indeholdt ugyldige data - se debug.log</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>Fejl ved indlæsning af %s: Du kan ikke aktivere HD på en allerede eksisterende ikke-HD-pung</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>Fejl ved læsning af %s! Alle nøgler læses korrekt, men transaktionsdata eller adressebogsposter kan mangle eller være forkerte.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>Fejl ved læsning fra møntdatabasen.
Detaljer: %s

Vil du genindeksere ved næste genstart?</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>Kunne ikke lytte på alle P2P-porte. Mislykkedes som anmodet af -bindallorfail.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>Gebyr: %ld er større end det konfigurerede maksimale tilladte gebyr på: %ld. For at ændre skal du indstille &apos;wallet.maxTxFee&apos;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>Ugyldigt beløb for -wallet.maxTxFee=&lt;beløb&gt;: &apos;%u&apos; (skal mindst være minrelay-gebyret på %s for at forhindre fastlåste transaktioner)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>Ugyldigt beløb for -wallet.payTxFee=&lt;beløb&gt;: &apos;%u&apos; (skal være mindst %s)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>Tjek venligst, at din computers dato og klokkeslæt er korrekte! Hvis dit ur er forkert, vil %s ikke fungere korrekt.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>Reducerer -next.max Connections fra %d til %d på grund af fildeskriptorbegrænsninger (unix) eller winsocket fd_set-begrænsninger (windows). Hvis du er Windows-bruger, er der en hård øvre grænse på 1024, som ikke kan ændres ved at justere nodens konfiguration.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>Samlet længde af netværksversionsstreng med tilføjede uacomments overskred den maksimale længde (%i) og er blevet afkortet. Reducer antallet eller størrelsen af ua-kommentarer for at undgå trunkering.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>Transaktionen har %d input og %d output. Maksimalt tilladte input er %d og maksimum output er %d</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>Transaktionen har %d input. Maksimalt tilladte input er %d. Prøv at reducere input ved at overføre et mindre beløb.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>Wallet er ikke beskyttet med adgangskode. Dine midler kan være i fare! Gå til &quot;Indstillinger&quot;, og vælg derefter &quot;Krypter tegnebog&quot; for at oprette en adgangskode.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>Advarsel: Kunne ikke åbne installationskonfigurations-CSV-filen &apos;%s&apos; til læsning</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>Advarsel: Ukendte blokversioner udvindes! Det er muligt, at ukendte regler er i kraft</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>Advarsel: Tegnebogsfil korrupt, data reddet! Original %s gemt som %s i %s; hvis din saldo eller dine transaktioner er forkerte, skal du gendanne fra en sikkerhedskopi.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>Du forsøger at bruge -wallet.auto, men hverken -spendzeroconfchange eller -wallet.instant er slået til</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to use wallet.fallbackFee which has been deprecated an no longer in  use - use wallet.payTxFee instead </source>
        <translation>Du forsøger at bruge wallet.fallbackFee, som er blevet forældet og ikke længere i brug - brug wallet.payTxFee i stedet for </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to use wallet.minTxFee which has been deprecated an no longer in  use - use wallet.payTxFee instead </source>
        <translation>Du forsøger at bruge wallet.minTxFee, som er blevet forældet og ikke længere i brug - brug wallet.payTxFee i stedet </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>Du kan ikke sende gratis transaktioner, hvis du har konfigureret et -relay.limitFreeRelay på nul</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s korrupt, redning mislykkedes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-cache.maxTxPool skal være mindst %d MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>-xthinbloomfiltersize skal være mindst %d bytes</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write default address</source>
        <translation>Kan ikke skrive standardadresse</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>Forpligtelsestransaktion mislykkedes.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>Installationskonfigurationsfilen &apos;%s&apos; blev ikke fundet</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation>Fejl ved indlæsning af %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>Fejl ved indlæsning af %s: Tegnebog er beskadiget</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>Fejl ved indlæsning af %s: Tegnebog  kræver en nyere version af %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>Fejl ved indlæsning af %s: Du kan ikke deaktivere HD på en allerede eksisterende HD-tegnebog</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>Initialiseringssundhedstjek mislykkedes. %s lukker ned.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>Utilstrækkelige midler eller midler ikke bekræftet</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>Keypool løb tør, ring venligst til keypoolrefill først</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>Indlæser Orphanpool</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>Indlæser TxPool</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation>Indlæser banliste...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Opening Block database...</source>
        <translation>Åbner blokdatabase...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>Åbner Coins Cache-database...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>Åbner UTXO-database...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>Dele Copyright (C) 2009-%i Bitcoin Core Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>Dele Copyright (C) 2014-%i Bitcoin XT-udviklerne</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>Genaccepter Wallet-transaktioner</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>Genindlæser…</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>Slå automatisk konsolidering fra, og prøv at sende igen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>Kan ikke binde til %s på denne computer. %s kører sandsynligvis allerede.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>Kan ikke starte RPC-tjenester. Se fejlretningslog for detaljer.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>Opgradering af blokdatabase...Dette kan tage et stykke tid.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>Opgradering af txindex database </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>Opgraderer txindex-database...</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>Tegnebog skulle omskrives: genstart %s for at fuldføre</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Done loading</source>
        <translation>Indlæsning gennemført</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>Fejl</translation>
    </message>
</context>
</TS>
